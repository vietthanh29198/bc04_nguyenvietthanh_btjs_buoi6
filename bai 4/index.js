function taoDiv() {
  var backGroundDiv = document.getElementById("bg");
  var allDiv = "";

  for (i = 1; i <= 10; i++) {
    var divRed = `<div class="p-3 bg-danger text-white">Div chẵn ${i}</div>`;
    var divBlue = `<div class="p-3 bg-primary text-white">Div lẻ ${i} </div>`;

    if (i % 2 == 0) {
      allDiv = allDiv + divRed;
    } else {
      allDiv = allDiv + divBlue;
    }
  }

  backGroundDiv.innerHTML = allDiv;
}
